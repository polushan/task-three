package task.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import task.model.Branch;
import task.service.BranchService;

import java.util.Optional;

@RestController
@RequestMapping("/branches")
public class BranchController {
    private BranchService branchService;

    @Autowired
    public BranchController(BranchService branchService) {
        this.branchService = branchService;
    }

    @RequestMapping("/{id}")
    public ResponseEntity<Branch> getBranchById(@PathVariable("id") Integer branchId) {
        Optional<Branch> branchOpt = branchService.getBranch(branchId);
        return branchOpt.map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
}
