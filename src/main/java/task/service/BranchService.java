package task.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import task.model.Branch;
import task.repository.BranchRepository;

import java.util.Optional;

@Service
public class BranchService {
    private BranchRepository branchRepository;

    @Autowired
    public BranchService(BranchRepository branchRepository) {
        this.branchRepository = branchRepository;
    }

    public Optional<Branch> getBranch(Integer branchId) {
        return branchRepository.findById(branchId);
    }
}
