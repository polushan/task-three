package task.repository;

import org.springframework.data.repository.CrudRepository;
import task.model.Branch;

public interface BranchRepository extends CrudRepository<Branch, Integer> {
}
